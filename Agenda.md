Git Session
- VCS Intro 
- Types of VC
- Difference between Centralized and Distributed 
- Advantages of Git 
- Local vs Remote 
- Git commands 
- Branching 
- Merge vs Rebase 
- Pull requests 