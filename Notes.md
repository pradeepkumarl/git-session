# Challenges of not Working with VCS
- Versioning
- Storing the files 
- Authentication and Authorization 
- Backup and Recovery 

# Properties of VC 
- Versioning
- Storing the files 
- Authentication and Authorization 
- Backup and Recovery 
- Maintains the Integrity of the codebase

# Types of Version Control System
- Centralized
  - SubVersion
  - Perforce
  - CVS 
- Distributed
  - Git
  - Mercurial

# Drawbacks Of Central Version Control
- Single point of failure 
- Creating branches for parallel development will be costly
- Should always be connected to the VC for any operation 
- Lacks scalability

# Advantages of Distributed VC 
- Speed
- All operations will be local
- Cheap branches
- Scalability

# Git Commands 

## Configuring Git
Git version 
```
git version
```
- Commit is a Log book 
Git records three attributes for every commit 

- Who did the change - Comes from Profile  
    - ```user.name``` and ```user.email```
- Why was the change made  - Commit message
- When was the change made - System time

To set the profile

```
git config --global user.name pradeep
git config --global user.email pradeep@glearning.com
```
## Other Git Commands 

Create a Git project 
```
  git init
```
To check the status of the files 
```
 git status
```
Moving from working directory to Staging area
```
 git add file1 file2 file3
 git add . 
```

Moving from staging area to Repository 
```
 git commit -m "Initial commit"
```

Moving from working directory to Repository
```
git commit -am "add and commit"
```

Check the Git log
```
git log
git log --oneline
```
Undo the change in the working directory
```
git checkout -- filename
git checkout -- . (to undo all the changes)
```

## Branching in Git

List branches
```
git branch
```
Create branch
```
git branch branch-name
```

Switch branch
```
git checkout branch-name
```
Create and checkout
```
git checkout -b branch-name
```

Cloning existing repository
```
git clone git@gitlab.com:pradeepkumarl/git-session.git
```

Push the changes to remote repository
```
git push origin master
```

Pull the changes to local repository
```
git pull origin master
```

Steps to raise the Pull request
1. Create a branch on the remote (Github/Gitlab)
2. Pull the branch to local using `git pull origin` command 
3. Make the changes in the branch
4. Commit the changes
5. Push the changes to the remote using `git push origin`
6. Go to Remote and Create a Pull request
7. Assign the pull request to the reviewer
8. The reviewer will merge the changes to the master
9. On local navigate to the master ```git checkout master```
10. Pull the latest changes ```git pull origin master```

## Merging strategies

1. Fast-Forward
    When there are no changes in the destination branch from the time the source branch is cut and till the source branch is merged with the destination branch 

2. `3-way` recursive merging strategy
   From the time of creating a branch and merging the changes to the destination branch, if there are any commits in the destination branch 


## Rebase commands 
1. To create a branch from master ```git checkout -b rebase-branch```
2. commit few changes in the `rebase-branch`
3. Checkout to master branch `git checkout master`
4. commit few changes in the `master` branch
5. Checkout to the `rebase-demo` branch
6. Rebase the current branch on top of `master` branch 
7. Apply all the changes
8. Checkout to `master` branch
9. Merge the changes from the `rebase-demo` branch

## Undo changes 
Unstage the changes from staging area
```
git reset HEAD filename
git reset HEAD . (for all the files)
```

## References
- [Reference-Link](https://gitlab.com/pradeepkumarl/git-session)
